const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');
const assert = require('assert');
const Sequelize = require('sequelize');

const all_ratings = ['G', 'PG', 'PG-13', 'R'];

var browser;
var page;
var database;
var port;
var number_of_movies;
var created_at_field = "created_at";

switch (process.env.TARGET) {
    case "rails":
        database = "hw1rails";
        port = "3000";
        break;
    case "phoenix":
        database = "hw1phoenix";
        created_at_field = "inserted_at";
        port = "4000";
        break;
    default:
        database = "hw1sails";
        port = "1337";
        break;
}

const sequelize = new Sequelize('postgres://postgres:postgres@localhost/' + database);

var Movie = sequelize.define('movie', {
    title: Sequelize.STRING,
    description: Sequelize.STRING,
    rating: Sequelize.STRING,
    release_date: Sequelize.DATE,
    createdAt: {type: Sequelize.DATE, field: created_at_field},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
});

Before(async function() {
    browser = await puppeteer.launch({headless: false});
});

After(async function() {
    await Movie.destroy({truncate: true});
    // await browser.close();
});

Given('the following movies exist:', async function (dataTable) {
    number_of_movies = dataTable.hashes().length;
    await Movie.bulkCreate(dataTable.hashes());
});
  
Given('I am on the RottenPotatoes home page', async function () {
    page = await browser.newPage();
    await page.goto(`http://localhost:${port}/movies`);
    await page.waitFor(1000);
});
  
When('I check the following ratings: {}', async function (ratings) {
    ratings = ratings.split(", ");
    all_ratings.forEach(async function (rating) {
        if (ratings.includes(rating)) {
            await page.$eval("#cb_" + rating, cb => cb.checked = true);
        } else if (!ratings.includes(ratings)) {
            await page.$eval("#cb_" + rating, cb => cb.checked = false);
        }
    });
});
  
When('I press {string}', async function (button_text) {
    const submit_button = await page.$x(`//button[contains(text(), '${button_text}')]`);
    // await submit_button[0].click();
    await page.waitFor(500);
});
  

When('I follow {string}', async function (button_text) {
    const submit_button = await page.$x(`//button[contains(text(), '${button_text}')]`);
    // await submit_button[0].click();
    await page.waitFor(500);
});

Then(/^I should (not )?see the following movies: (.+)$/, async function (not_seen, movies) {
    const movie_rows = await page.$x(`//table/tbody/tr`);
    let titulos = movie_rows.map(item=>{ 
        return item._remoteObject.description.substring(3);
    })
    console.log(titulos);
    if(movies){
        titulos.forEach(movie=>{
            assert.equal(movies.includes(movies), true);
        });
    }
    else {
        titulos.forEach(movie=>{
            assert.equal(!movies.includes(movies), false);
        })
    }
    console.log("\nMovies", movies.split(", "));
    console.log(not_seen);
});
  
Then('I should see all the movies', async function () {
    const movie_rows = await page.$x(`//table/tbody/tr`);
    await page.waitFor(1000);
    assert.equal(movie_rows.length, number_of_movies);
});

Then('I should see {string} before {string} in the movie list', async function(title) {
    const movie_rows = await page.$x(`//table/tbody/tr`);
    await page.waitFor(1000);
    let titulos = movie_rows.map(item=>{ 
        return item._remoteObject.description.substring(3);
    })
    assert.equal(movie_rows.length, number_of_movies);
    // let fecha = movie_rows.map(item=>{ 
    //     return item._remoteObject.description.substring(3);
    // })
    // fecha.sort(compareValues('release_date'));

});