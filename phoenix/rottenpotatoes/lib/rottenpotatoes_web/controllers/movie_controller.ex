defmodule RottenpotatoesWeb.MovieController do
  use RottenpotatoesWeb, :controller

  alias Rottenpotatoes.{Repo, Listing.Movie}

  @all_ratings ~w[G PG PG-13 R]

  def index(conn, _params) do
    #query = "from movie in Movie, where: movie.rating == @selected_ratings"
    movies = Repo.all()
    render(
      conn, "index.html", 
      movies: movies, 
      ratings: @all_ratings, 
      selected_ratings: %{"G" => "on"}, 
      sort_by: ""
    )
  end
end
