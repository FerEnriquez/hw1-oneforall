defmodule Rottenpotatoes.Repo do
  use Ecto.Repo,
    otp_app: :rottenpotatoes,
    adapter: Ecto.Adapters.Postgres
end
