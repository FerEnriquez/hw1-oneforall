You will notice that the repository includes four folders. To complete the setup, do the following:

For our acceptance test (cucumberjs):

cd acceptance
npm install
For our Sails project:

cd ..
cd sails
cd rottenpotatoes
npm install
For our Ruby on Rails project:

cd ..
cd ..
cd rails
cd rottenpotatoes
bundle install
rake app:update:bin
rake db:migrate //crear db en postgres hw1rails
For our Elixir/Phoenix project:

cd ..
cd ..
cd phoenix
cd rottenpotatoes
cd assets
npm install
node node_modules/webpack/bin/webpack.js --mode development
cd ..
mix deps.get
mix ecto.setup
NOTE
I am assuming PostgreSQL is up and running, listening the default port.