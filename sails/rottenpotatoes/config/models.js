module.exports.models = {
  schema: true,
  migrate: 'drop',
  attributes: {
    createdAt: { type: 'ref', columnType: 'timestamp', autoCreatedAt: true, columnName: 'created_at'},
    updatedAt: { type: 'ref', columnType: 'timestamp', autoUpdatedAt: true, columnName: 'updated_at'},
    id: { type: 'number', autoIncrement: true, },
  },
  dataEncryptionKeys: {
    default: 'ZOfkEu9TU83uPt+se67l9FPGQHPgYPHCJDROYWiEpto='
  },
  cascadeOnDestroy: true
};
