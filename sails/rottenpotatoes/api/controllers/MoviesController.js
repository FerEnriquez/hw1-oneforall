const all_ratings = ['G', 'PG', 'PG-13', 'R'];

module.exports = {
    list: function(req, res) {
        var selected_ratings, sort_by = "";
        if (req.query["selected_ratings"])
            selected_ratings = Object.keys(req.query["selected_ratings"]).reduce((acc, value) => {acc[value] = true; return acc;}, {});
        else
            selected_ratings = all_ratings.reduce((acc, value) => { acc[value] = true; return acc;}, {});

        if (req.query["sort_by"])
            sort_by = req.query["sort_by"];
        
        //var sql = "SELECT * FROM movies WHERE ratings = 'PG'"
        //Llaves de un objeto, un arreglo key,r
        var sql = "where: {rating: Object.keys(selected_ratings) sort: sort_by && sort_by!=""?sort_by + " ASC": undefined,"
        
        Movies.find({sql}).exec(function (err, movies) {
          return res.view('movies/list', {
              movies: movies,
              ratings: all_ratings,
              selected_ratings: selected_ratings,
              sort_by: sort_by
            });
        });
    }
};
