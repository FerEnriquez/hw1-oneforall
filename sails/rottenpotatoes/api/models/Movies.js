module.exports = {
  attributes: {
    title: {type: 'string'},
    description: {type: 'string', allowNull: true},
    rating: {type: 'string'},
    release_date: {type: 'ref', columnType: 'timestamp'}
  }
};

