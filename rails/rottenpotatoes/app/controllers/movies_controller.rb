class MoviesController < ApplicationController
  def index
      params[:selected_ratings] ? params[:selected_ratings].keys : ["G","PG","PG-13","R"]; 
      params[:sort_by] && params[:sort_by] != "" ? "#{params[:sort_by]} ASC" : ""; 
      sort = params[:sort_by] && params[:sort_by] != "" ? 
      
      @ratings = Movie.all_ratings
      @selected_ratings = params[:ratings] || session[:ratings] || {}
      
      if @selected_ratings == {}
        @selected_ratings = Hash[@ratings.map {|rating| [rating, rating]}]
      end
      
      @movies = Movie.where(rating: @selected_ratings.keys).order(sort)
  end
endg